void r8mat_mv_ext_line256(int m, int n, double  a[], double  x[], double  ax[])
{
    int i;
    int j;
    for(i = 0; i < m; i++)
    {
        ax[i] = 0.0;
        for(j = 0; j < n; j++)
        {
            ax[i] = ax[i] + a[i + j * m] * x[j];
        }
    }
}
