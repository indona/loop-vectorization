void r8mat_mv_ext_line256(int m, int n, double  a[], double  x[], double  ax[])
{
    int i;
    int j;
    // ax - array
    for(i = 0; i < m; i++)
    {
        ax[i] = 0.0;
        for(j = 0; j < n; j++)
        {
            ax[i] = ax[i] + a[i + j * m] * x[j];
        }
    }
}

int main()
{
    int m = 7000, n = 800;
    double a[100] = {3, 100, 999}, x[100] = {55, 69}, ax[100] = {33, 180, 160, 1};
    int job = 80;

    r8mat_mv_ext_line256(m, n, a, x, ax);
    return 0;
}
