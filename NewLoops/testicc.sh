#!/bin/bash
for file in *.c
do
    echo Working on $file
    ./yyyicc.sh $file
    if grep -Fq "LOOP WAS VECTORIZED" $file.optrpt
    then
        echo $file IS VECTORIZABLE
        echo $file >> vectorizableicc
    else
        echo $file >> iccunvectorizable
    fi
done

