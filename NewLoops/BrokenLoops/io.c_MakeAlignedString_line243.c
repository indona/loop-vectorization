#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/alignio.c_MakeAlignedString_line243_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int apos;
int rpos;
int alen;
char * aseq;
char * new;
char * ss;

void loop()
{
#pragma scop

    for(apos = rpos = 0; apos < alen; apos++) if(!(aseq[apos] == 32 || aseq[apos] == '.' || aseq[apos] == '_' || aseq[apos] == '-' || aseq[apos] == '~'))
        {
            new[apos] = ss[rpos];
            rpos++;
        }
        else new[apos] = '.';

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    memcpy(&alen, &alen_, sizeof(alen_));

    int aseq_dimension_sizes[1];
    aseq_dimension_sizes[0] = sizeof(aseq_)/sizeof(*aseq_);
    aseq = (char *) malloc (sizeof(char) * aseq_dimension_sizes[0]);

    /*----------------- read data for aseq----------------------*/
    char char_storage;
    pFile_ = fopen("../dump/dataFile/alignio.c_MakeAlignedString_line243_1.aseq.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_MakeAlignedString_line243_1.aseq.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        aseq_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for aseq------------------*/
    aseq = aseq_;

    int new_dimension_sizes[1];
    new_dimension_sizes[0] = sizeof(new_)/sizeof(*new_);
    new = (char *) malloc (sizeof(char) * new_dimension_sizes[0]);


    int ss_dimension_sizes[1];
    ss_dimension_sizes[0] = sizeof(ss_)/sizeof(*ss_);
    ss = (char *) malloc (sizeof(char) * ss_dimension_sizes[0]);

    /*----------------- read data for ss----------------------*/
    pFile_ = fopen("../dump/dataFile/alignio.c_MakeAlignedString_line243_1.ss.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_MakeAlignedString_line243_1.ss.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        ss_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for ss------------------*/
    ss = ss_;

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    unsigned char MD5_new[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_new;
    MD5_Init (&mdContext_new);
    for (int i0 = 0; i0 < sizeof(new_)/sizeof(new_[0]); i0++)
    {
        MD5_Update(&mdContext_new, &new[i0], sizeof(char));
    }
    MD5_Final(MD5_new, &mdContext_new);
    printf("MD5_new: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_new[i]);
    }
    printf("\n");

    printf("\n");
    printf("apos: %d\n", apos);
    printf("rpos: %d\n", rpos);

    return 0;
}
