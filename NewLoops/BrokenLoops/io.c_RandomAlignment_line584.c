#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/alignio.c_RandomAlignment_line584_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int idx;
int nseq;
int apos;
int rpos;
int statepos;
int M;
int count;
int ** ins;
char ** aseqs;
char ** rseqs;
int * master_ins;
int alen;

void loop()
{
#pragma scop

    for(idx = 0; idx < nseq; idx++)
    {
        apos = rpos = 0;
        for(statepos = 0; statepos <= M; statepos++)
        {
            for(count = 0; count < ins[idx][statepos]; count++) aseqs[idx][apos++] = rseqs[idx][rpos++];
            for(; count < master_ins[statepos]; count++) aseqs[idx][apos++] = 32;
            if(statepos != M) aseqs[idx][apos++] = rseqs[idx][rpos++];
        }
        aseqs[idx][alen] = '\0';
    }

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    memcpy(&nseq, &nseq_, sizeof(nseq_));
    memcpy(&M, &M_, sizeof(M_));

    int ins_dimension_sizes[2];
    ins_dimension_sizes[0] = sizeof(ins_)/sizeof(*ins_);
    ins_dimension_sizes[1] = sizeof(*ins_)/sizeof(**ins_);
    ins = (int **) malloc (sizeof(int *) * ins_dimension_sizes[0]);

    /*----------------- read data for ins----------------------*/
    int int_storage;
    pFile_ = fopen("../dump/dataFile/alignio.c_RandomAlignment_line584_1.ins.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_RandomAlignment_line584_1.ins.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_, "%d", &inputIndices[1]);
        fscanf(pFile_,"%d", &int_storage);
        ins_[inputIndices[0]][inputIndices[1]] = int_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for ins------------------*/
    for (int dimension_i0= 0; dimension_i0 < ins_dimension_sizes[0]; dimension_i0++)
    {
        ins[dimension_i0] = (int *) malloc (sizeof(int) *ins_dimension_sizes[1]);
        memcpy(ins[dimension_i0], ins_[dimension_i0], sizeof(ins_[dimension_i0]));
    }

    int aseqs_dimension_sizes[2];
    aseqs_dimension_sizes[0] = sizeof(aseqs_)/sizeof(*aseqs_);
    aseqs_dimension_sizes[1] = sizeof(*aseqs_)/sizeof(**aseqs_);
    aseqs = (char **) malloc (sizeof(char *) * aseqs_dimension_sizes[0]);

    for (int dimension_i0= 0; dimension_i0 < aseqs_dimension_sizes[0]; dimension_i0++)
    {
        aseqs[dimension_i0] = (char *) malloc (sizeof(char) *aseqs_dimension_sizes[1]);
    }

    int rseqs_dimension_sizes[2];
    rseqs_dimension_sizes[0] = sizeof(rseqs_)/sizeof(*rseqs_);
    rseqs_dimension_sizes[1] = sizeof(*rseqs_)/sizeof(**rseqs_);
    rseqs = (char **) malloc (sizeof(char *) * rseqs_dimension_sizes[0]);

    /*----------------- read data for rseqs----------------------*/
    char char_storage;
    pFile_ = fopen("../dump/dataFile/alignio.c_RandomAlignment_line584_1.rseqs.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_RandomAlignment_line584_1.rseqs.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_, "%d", &inputIndices[1]);
        fscanf(pFile_,"%c", &char_storage);
        rseqs_[inputIndices[0]][inputIndices[1]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for rseqs------------------*/
    for (int dimension_i0= 0; dimension_i0 < rseqs_dimension_sizes[0]; dimension_i0++)
    {
        rseqs[dimension_i0] = (char *) malloc (sizeof(char) *rseqs_dimension_sizes[1]);
        memcpy(rseqs[dimension_i0], rseqs_[dimension_i0], sizeof(rseqs_[dimension_i0]));
    }

    int master_ins_dimension_sizes[1];
    master_ins_dimension_sizes[0] = sizeof(master_ins_)/sizeof(*master_ins_);
    master_ins = (int *) malloc (sizeof(int) * master_ins_dimension_sizes[0]);

    /*----------------- read data for master_ins----------------------*/
    pFile_ = fopen("../dump/dataFile/alignio.c_RandomAlignment_line584_1.master_ins.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_RandomAlignment_line584_1.master_ins.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%d", &int_storage);
        master_ins_[inputIndices[0]] = int_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for master_ins------------------*/
    master_ins = master_ins_;
    memcpy(&alen, &alen_, sizeof(alen_));

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    unsigned char MD5_aseqs[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_aseqs;
    MD5_Init (&mdContext_aseqs);
    for (int i0 = 0; i0 < aseqs_dimension_sizes[0]; i0++)
    {
        for (int i1 = 0; i1 < aseqs_dimension_sizes[1]; i1++)
        {
            MD5_Update(&mdContext_aseqs, &aseqs[i0][i1], sizeof(char));
        }
    }
    MD5_Final(MD5_aseqs, &mdContext_aseqs);
    printf("MD5_aseqs: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_aseqs[i]);
    }
    printf("\n");

    printf("\n");
    printf("idx: %d\n", idx);
    printf("apos: %d\n", apos);
    printf("rpos: %d\n", rpos);
    printf("statepos: %d\n", statepos);
    printf("count: %d\n", count);

    return 0;
}
