#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/aligneval.c_make_ref_alilist_line259_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int col;
char * s1;
int * canons1;
int r1;
int * s1_list;
int lpos;
char * s2;
int r2;

void loop()
{
#pragma scop

    for(col = 0; s1[col] != '\0'; col++)
    {
        if(!(s1[col] == 32 || s1[col] == '.' || s1[col] == '_' || s1[col] == '-' || s1[col] == '~') && canons1[r1])
        {
            s1_list[lpos] =(s2[col] == 32 || s2[col] == '.' || s2[col] == '_' || s2[col] == '-' || s2[col] == '~'?- 1 : r2);
            lpos++;
        }
        if(!(s1[col] == 32 || s1[col] == '.' || s1[col] == '_' || s1[col] == '-' || s1[col] == '~')) r1++;
        if(!(s2[col] == 32 || s2[col] == '.' || s2[col] == '_' || s2[col] == '-' || s2[col] == '~')) r2++;
    }

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;

    int s1_dimension_sizes[1];
    s1_dimension_sizes[0] = sizeof(s1_)/sizeof(*s1_);
    s1 = (char *) malloc (sizeof(char) * s1_dimension_sizes[0]);

    /*----------------- read data for s1----------------------*/
    char char_storage;
    pFile_ = fopen("../dump/dataFile/aligneval.c_make_ref_alilist_line259_1.s1.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/aligneval.c_make_ref_alilist_line259_1.s1.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        s1_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for s1------------------*/
    s1 = s1_;

    int canons1_dimension_sizes[1];
    canons1_dimension_sizes[0] = sizeof(canons1_)/sizeof(*canons1_);
    canons1 = (int *) malloc (sizeof(int) * canons1_dimension_sizes[0]);

    /*----------------- read data for canons1----------------------*/
    int int_storage;
    pFile_ = fopen("../dump/dataFile/aligneval.c_make_ref_alilist_line259_1.canons1.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/aligneval.c_make_ref_alilist_line259_1.canons1.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%d", &int_storage);
        canons1_[inputIndices[0]] = int_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for canons1------------------*/
    canons1 = canons1_;
    memcpy(&r1, &r1_, sizeof(r1_));

    int s1_list_dimension_sizes[1];
    s1_list_dimension_sizes[0] = sizeof(s1_list_)/sizeof(*s1_list_);
    s1_list = (int *) malloc (sizeof(int) * s1_list_dimension_sizes[0]);

    memcpy(&lpos, &lpos_, sizeof(lpos_));

    int s2_dimension_sizes[1];
    s2_dimension_sizes[0] = sizeof(s2_)/sizeof(*s2_);
    s2 = (char *) malloc (sizeof(char) * s2_dimension_sizes[0]);

    /*----------------- read data for s2----------------------*/
    pFile_ = fopen("../dump/dataFile/aligneval.c_make_ref_alilist_line259_1.s2.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/aligneval.c_make_ref_alilist_line259_1.s2.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        s2_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for s2------------------*/
    s2 = s2_;
    memcpy(&r2, &r2_, sizeof(r2_));

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    unsigned char MD5_s1_list[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_s1_list;
    MD5_Init (&mdContext_s1_list);
    for (int i0 = 0; i0 < sizeof(s1_list_)/sizeof(s1_list_[0]); i0++)
    {
        MD5_Update(&mdContext_s1_list, &s1_list[i0], sizeof(int));
    }
    MD5_Final(MD5_s1_list, &mdContext_s1_list);
    printf("MD5_s1_list: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_s1_list[i]);
    }
    printf("\n");

    printf("\n");
    printf("col: %d\n", col);
    printf("r1: %d\n", r1);
    printf("lpos: %d\n", lpos);
    printf("r2: %d\n", r2);

    return 0;
}
