#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/alpha.c_gl_alpha_test_line111_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);


typedef unsigned int GLuint;
GLuint i;
GLuint n;
typedef unsigned char GLubyte;
unsigned char (*mask);
const GLubyte (*alpha);
GLubyte ref;

void loop()
{
#pragma scop

    for(i = 0; i < n; i++)
    {
        mask[i] &= alpha[i] >= ref;
    }

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    memcpy(&n, &n_, sizeof(n_));
    int mask_dimension_sizes[1];
    /*----------------- read data for mask----------------------*/
    unsigned char unsigned_char_storage;
    pFile_ = fopen("../dump/dataFile/alpha.c_gl_alpha_test_line111_1.mask.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alpha.c_gl_alpha_test_line111_1.mask.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%u", &unsigned_char_storage);
        mask_[inputIndices[0]] = unsigned_char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for mask------------------*/
    mask = mask_;
    /*----------------- read data for alpha----------------------*/
    pFile_ = fopen("../dump/dataFile/alpha.c_gl_alpha_test_line111_1.alpha.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alpha.c_gl_alpha_test_line111_1.alpha.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%u", &unsigned_char_storage);
        alpha_[inputIndices[0]] = unsigned_char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for alpha------------------*/
    alpha = alpha_;
    memcpy(&ref, &ref_, sizeof(ref_));

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    unsigned char MD5_mask[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_mask;
    MD5_Init (&mdContext_mask);
    for (int i0 = 0; i0 < sizeof(mask_)/sizeof(mask_[0]); i0++)
    {
        MD5_Update(&mdContext_mask, &mask[i0], sizeof(unsigned char));
    }
    MD5_Final(MD5_mask, &mdContext_mask);
    printf("MD5_mask: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_mask[i]);
    }
    printf("\n");

    printf("\n");
    printf("i: %u\n", i);

    return 0;
}
