#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
//#include "../dump/aligneval.c_PairwiseIdentity_line408_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int x;
char * s1;
char * s2;
int len1;
int idents;
int len2;

void loop()
{
//#pragma scop

    for(x = 0; s1[x] != '\0' && s2[x] != '\0'; x++)
    {
        if(!(s1[x] == 32 || s1[x] == '.' || s1[x] == '_' || s1[x] == '-' || s1[x] == '~'))
        {
            len1++;
            if(s1[x] == s2[x]) idents++;
        }
        if(!(s2[x] == 32 || s2[x] == '.' || s2[x] == '_' || s2[x] == '-' || s2[x] == '~')) len2++;
    }

//#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;

    int s1_dimension_sizes[1];
    s1_dimension_sizes[0] = sizeof(s1_)/sizeof(*s1_);
    s1 = (char *) malloc (sizeof(char) * s1_dimension_sizes[0]);

    /*----------------- read data for s1----------------------*/
    char char_storage;
    pFile_ = fopen("../dump/dataFile/aligneval.c_PairwiseIdentity_line408_1.s1.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/aligneval.c_PairwiseIdentity_line408_1.s1.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        s1_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for s1------------------*/
    s1 = s1_;

    int s2_dimension_sizes[1];
    s2_dimension_sizes[0] = sizeof(s2_)/sizeof(*s2_);
    s2 = (char *) malloc (sizeof(char) * s2_dimension_sizes[0]);

    /*----------------- read data for s2----------------------*/
    pFile_ = fopen("../dump/dataFile/aligneval.c_PairwiseIdentity_line408_1.s2.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/aligneval.c_PairwiseIdentity_line408_1.s2.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        s2_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for s2------------------*/
    s2 = s2_;
    memcpy(&len1, &len1_, sizeof(len1_));
    memcpy(&idents, &idents_, sizeof(idents_));
    memcpy(&len2, &len2_, sizeof(len2_));

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    printf("\n");
    printf("x: %d\n", x);
    printf("len1: %d\n", len1);
    printf("idents: %d\n", idents);
    printf("len2: %d\n", len2);

    return 0;
}
