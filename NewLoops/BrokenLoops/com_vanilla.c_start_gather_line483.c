#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/com_vanilla.c_start_gather_line483_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int j;
int even_sites_on_node;
typedef struct
{
    short x;
    short y;
    short z;
    short t;
    char parity;
    int index;
    double_prn site_prn;
    int space1;
    su3_matrix link site[4];
    anti_hermitmat mom[4];
    double phase[4];
    su3_vector phi;
    su3_vector resid;
    su3_vector cg_p;
    su3_vector xxx;
    su3_vector ttt;
    su3_vector g_rand;
    su3_vector tempvec[4];
    su3_vector templongvec[4];
    su3_vector templongv1;
    su3_matrix tempmat1;
    su3_matrix staple;
};
site * s;
site * lattice;
int sites_on_node;
char ** dest;
int ** neighbor;
int index;
typedef int field_offset;
field_offset field;

void loop()
{
#pragma scop

    for((j = even_sites_on_node , s = &lattice[j]); j < sites_on_node; (j++ , s++))
    {
        dest[j] =((char *)(lattice + neighbor[index][j])) + field;
    }

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    memcpy(&j, &j_, sizeof(j_));
    memcpy(&even_sites_on_node, &even_sites_on_node_, sizeof(even_sites_on_node_));

    int s_dimension_sizes[1];
    s_dimension_sizes[0] = sizeof(s_)/sizeof(*s_);
    s = (site *) malloc (sizeof(site) * s_dimension_sizes[0]);

    /*----------------- read data for s----------------------*/
    struct
    {
        short x;
        short y;
        short z;
        short t;
        char parity;
        int index;
        double_prn site_prn;
        int space1;
        su3_matrix link[4];
        anti_hermitmat mom[4];
        double phase[4];
        su3_vector phi;
        su3_vector resid;
        su3_vector cg_p;
        su3_vector xxx;
        su3_vector ttt;
        su3_vector g_rand;
        su3_vector tempvec[4];
        su3_vector templongvec[4];
        su3_vector templongv1;
        su3_matrix tempmat1;
        su3_matrix staple;
    } struct_{short_x; short_y; short_z; short_t; char_parity; int_index; double_prn_site_prn; int_space1; su3_matrix_link[4]; anti_hermitmat_mom[4]; double_phase[4]; su3_vector_phi; su3_vector_resid; su3_vector_cg_p; su3_vector_xxx; su3_vector_ttt; su3_vector_g_rand; su3_vector_tempvec[4]; su3_vector_templongvec[4]; su3_vector_templongv1; su3_matrix_tempmat1; su3_matrix_staple;} _storage;
    pFile_ = fopen("../dump/dataFile/com_vanilla.c_start_gather_line483_1.s.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/com_vanilla.c_start_gather_line483_1.s.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"", &struct_{short_x; short_y; short_z; short_t; char_parity; int_index; double_prn_site_prn; int_space1; su3_matrix_link[4]; anti_hermitmat_mom[4]; double_phase[4]; su3_vector_phi; su3_vector_resid; su3_vector_cg_p; su3_vector_xxx; su3_vector_ttt; su3_vector_g_rand; su3_vector_tempvec[4]; su3_vector_templongvec[4]; su3_vector_templongv1; su3_matrix_tempmat1; su3_matrix_staple;} _storage);
        s_[inputIndices[0]] = struct_{short_x; short_y; short_z; short_t; char_parity; int_index; double_prn_site_prn; int_space1; su3_matrix_link[4]; anti_hermitmat_mom[4]; double_phase[4]; su3_vector_phi; su3_vector_resid; su3_vector_cg_p; su3_vector_xxx; su3_vector_ttt; su3_vector_g_rand; su3_vector_tempvec[4]; su3_vector_templongvec[4]; su3_vector_templongv1; su3_matrix_tempmat1; su3_matrix_staple;} _storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for s------------------*/
    s = s_;

    int lattice_dimension_sizes[1];
    lattice_dimension_sizes[0] = sizeof(lattice_)/sizeof(*lattice_);
    lattice = (site *) malloc (sizeof(site) * lattice_dimension_sizes[0]);

    /*----------------- read data for lattice----------------------*/
    pFile_ = fopen("../dump/dataFile/com_vanilla.c_start_gather_line483_1.lattice.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/com_vanilla.c_start_gather_line483_1.lattice.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"", &struct_{short_x; short_y; short_z; short_t; char_parity; int_index; double_prn_site_prn; int_space1; su3_matrix_link[4]; anti_hermitmat_mom[4]; double_phase[4]; su3_vector_phi; su3_vector_resid; su3_vector_cg_p; su3_vector_xxx; su3_vector_ttt; su3_vector_g_rand; su3_vector_tempvec[4]; su3_vector_templongvec[4]; su3_vector_templongv1; su3_matrix_tempmat1; su3_matrix_staple;} _storage);
        lattice_[inputIndices[0]] = struct_{short_x; short_y; short_z; short_t; char_parity; int_index; double_prn_site_prn; int_space1; su3_matrix_link[4]; anti_hermitmat_mom[4]; double_phase[4]; su3_vector_phi; su3_vector_resid; su3_vector_cg_p; su3_vector_xxx; su3_vector_ttt; su3_vector_g_rand; su3_vector_tempvec[4]; su3_vector_templongvec[4]; su3_vector_templongv1; su3_matrix_tempmat1; su3_matrix_staple;} _storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for lattice------------------*/
    lattice = lattice_;
    memcpy(&sites_on_node, &sites_on_node_, sizeof(sites_on_node_));

    int dest_dimension_sizes[2];
    dest_dimension_sizes[0] = sizeof(dest_)/sizeof(*dest_);
    dest_dimension_sizes[1] = sizeof(*dest_)/sizeof(**dest_);
    dest = (char **) malloc (sizeof(char *) * dest_dimension_sizes[0]);

    for (int dimension_i0= 0; dimension_i0 < dest_dimension_sizes[0]; dimension_i0++)
    {
        dest[dimension_i0] = (char *) malloc (sizeof(char) *dest_dimension_sizes[1]);
    }

    int neighbor_dimension_sizes[2];
    neighbor_dimension_sizes[0] = sizeof(neighbor_)/sizeof(*neighbor_);
    neighbor_dimension_sizes[1] = sizeof(*neighbor_)/sizeof(**neighbor_);
    neighbor = (int **) malloc (sizeof(int *) * neighbor_dimension_sizes[0]);

    /*----------------- read data for neighbor----------------------*/
    int int_storage;
    pFile_ = fopen("../dump/dataFile/com_vanilla.c_start_gather_line483_1.neighbor.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/com_vanilla.c_start_gather_line483_1.neighbor.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_, "%d", &inputIndices[1]);
        fscanf(pFile_,"%d", &int_storage);
        neighbor_[inputIndices[0]][inputIndices[1]] = int_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for neighbor------------------*/
    for (int dimension_i0= 0; dimension_i0 < neighbor_dimension_sizes[0]; dimension_i0++)
    {
        neighbor[dimension_i0] = (int *) malloc (sizeof(int) *neighbor_dimension_sizes[1]);
        memcpy(neighbor[dimension_i0], neighbor_[dimension_i0], sizeof(neighbor_[dimension_i0]));
    }
    memcpy(&index, &index_, sizeof(index_));
    memcpy(&field, &field_, sizeof(field_));

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    unsigned char MD5_s[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_s;
    MD5_Init (&mdContext_s);
    for (int i0 = 0; i0 < sizeof(s_)/sizeof(s_[0]); i0++)
    {
        MD5_Update(&mdContext_s, &s[i0], sizeof(site));
    }
    MD5_Final(MD5_s, &mdContext_s);
    printf("MD5_s: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_s[i]);
    }
    printf("\n");

    unsigned char MD5_dest[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_dest;
    MD5_Init (&mdContext_dest);
    for (int i0 = 0; i0 < dest_dimension_sizes[0]; i0++)
    {
        for (int i1 = 0; i1 < dest_dimension_sizes[1]; i1++)
        {
            MD5_Update(&mdContext_dest, &dest[i0][i1], sizeof(char));
        }
    }
    MD5_Final(MD5_dest, &mdContext_dest);
    printf("MD5_dest: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_dest[i]);
    }
    printf("\n");

    printf("\n");
    printf("j: %d\n", j);

    return 0;
}
