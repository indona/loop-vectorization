#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/alignio.c_WritePairwiseAlignment_line384_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int apos;
int count1;
int count2;
char  buf1[61] = {0};
char  buf2[61] = {0};
char  bufmid[61] = {0};
int ** pam;

void loop()
{
#pragma scop

    for(apos = 0; apos < count1 && apos < count2; apos++)
    {
        if(!(buf1[apos] == 32 || buf1[apos] == '.' || buf1[apos] == '_' || buf1[apos] == '-' || buf1[apos] == '~') && !(buf2[apos] == 32 || buf2[apos] == '.' || buf2[apos] == '_' || buf2[apos] == '-' || buf2[apos] == '~'))
        {
            if(buf1[apos] == buf2[apos]) bufmid[apos] = buf1[apos];
            else if(pam[buf1[apos] - 'A'][buf2[apos] - 'A'] > 0) bufmid[apos] = '+';
            else bufmid[apos] = 32;
        }
        else bufmid[apos] = 32;
    }

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    memcpy(&count1, &count1_, sizeof(count1_));
    memcpy(&count2, &count2_, sizeof(count2_));
    /*----------------- read data for buf1----------------------*/
    char char_storage;
    pFile_ = fopen("../dump/dataFile/alignio.c_WritePairwiseAlignment_line384_1.buf1.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_WritePairwiseAlignment_line384_1.buf1.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        buf1_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for buf1------------------*/
    memcpy(buf1, buf1_, sizeof(buf1_));
    /*----------------- read data for buf2----------------------*/
    pFile_ = fopen("../dump/dataFile/alignio.c_WritePairwiseAlignment_line384_1.buf2.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_WritePairwiseAlignment_line384_1.buf2.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        buf2_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for buf2------------------*/
    memcpy(buf2, buf2_, sizeof(buf2_));
    int bufmid_dimension_sizes[1];

    int pam_dimension_sizes[2];
    pam_dimension_sizes[0] = sizeof(pam_)/sizeof(*pam_);
    pam_dimension_sizes[1] = sizeof(*pam_)/sizeof(**pam_);
    pam = (int **) malloc (sizeof(int *) * pam_dimension_sizes[0]);

    /*----------------- read data for pam----------------------*/
    int int_storage;
    pFile_ = fopen("../dump/dataFile/alignio.c_WritePairwiseAlignment_line384_1.pam.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_WritePairwiseAlignment_line384_1.pam.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_, "%d", &inputIndices[1]);
        fscanf(pFile_,"%d", &int_storage);
        pam_[inputIndices[0]][inputIndices[1]] = int_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for pam------------------*/
    for (int dimension_i0= 0; dimension_i0 < pam_dimension_sizes[0]; dimension_i0++)
    {
        pam[dimension_i0] = (int *) malloc (sizeof(int) *pam_dimension_sizes[1]);
        memcpy(pam[dimension_i0], pam_[dimension_i0], sizeof(pam_[dimension_i0]));
    }

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    unsigned char MD5_bufmid[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_bufmid;
    MD5_Init (&mdContext_bufmid);
    for (int i0 = 0; i0 < 61; i0++)
    {
        MD5_Update(&mdContext_bufmid, &bufmid[i0], sizeof(char));
    }
    MD5_Final(MD5_bufmid, &mdContext_bufmid);
    printf("MD5_bufmid: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_bufmid[i]);
    }
    printf("\n");

    printf("\n");
    printf("apos: %d\n", apos);

    return 0;
}
