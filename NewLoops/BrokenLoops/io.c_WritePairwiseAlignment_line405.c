#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/alignio.c_WritePairwiseAlignment_line405_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int apos;
int count2;
char  buf2[61] = {0};
int rawcount2;

void loop()
{
#pragma scop

    for(apos = 0; apos < count2; apos++) if(!(buf2[apos] == 32 || buf2[apos] == '.' || buf2[apos] == '_' || buf2[apos] == '-' || buf2[apos] == '~')) rawcount2++;

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    memcpy(&count2, &count2_, sizeof(count2_));
    /*----------------- read data for buf2----------------------*/
    char char_storage;
    pFile_ = fopen("../dump/dataFile/alignio.c_WritePairwiseAlignment_line405_1.buf2.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/alignio.c_WritePairwiseAlignment_line405_1.buf2.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%c", &char_storage);
        buf2_[inputIndices[0]] = char_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for buf2------------------*/
    memcpy(buf2, buf2_, sizeof(buf2_));
    memcpy(&rawcount2, &rawcount2_, sizeof(rawcount2_));

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    printf("\n");
    printf("apos: %d\n", apos);
    printf("rawcount2: %d\n", rawcount2);

    return 0;
}
