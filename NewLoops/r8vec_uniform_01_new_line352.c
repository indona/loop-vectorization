void r8vec_uniform_01_new_ext_line352(int n, int * seed)
{
    int i;
    int k;
    int i4_huge = 2147483647;
    double *r;
    for(i = 0; i < n; i++)
    {
        k = *seed / 127773;
        *seed = 16807 *( *seed - k * 127773) - k * 2836;
        if( *seed < 0)
        {
            *seed = *seed + i4_huge;
        }
        r[i] =((double )( *seed)) * 4.656612875E-10;
    }
}

int main()
{
    int n = 7000, m = 100;
    int * seed = &m;

    r8vec_uniform_01_new_ext_line352(n, seed);
    return 0;
}
