#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/combination.c_get_aa_status_line555_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);

int k;
int num_stones;
int  aa_status[(19 + 1) *(19 + 1)] = {0};
int  stones[19 * 19] = {0};

void loop()
{
//#pragma scope

        for(k = 0; k < num_stones; k++){
            if(aa_status[stones[k]] != 3){
                printf("%d", aa_status[stones[k]]);
                return;
            }
        }

//#pragma endscope
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    memcpy(&num_stones, &num_stones_, sizeof(num_stones_));
    /*----------------- read data for aa_status----------------------*/
    int int_storage;
    pFile_ = fopen("../dump/dataFile/combination.c_get_aa_status_line555_1.aa_status.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/combination.c_get_aa_status_line555_1.aa_status.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%d", &int_storage);
        aa_status_[inputIndices[0]] = int_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for aa_status------------------*/
    memcpy(aa_status, aa_status_, sizeof(aa_status_));
    /*----------------- read data for stones----------------------*/
    pFile_ = fopen("../dump/dataFile/combination.c_get_aa_status_line555_1.stones.dat", "r");
    if (pFile_ == NULL)
    {
        printf("data file: ../dump/dataFile/combination.c_get_aa_status_line555_1.stones.dat does not exist!");
        exit(-1);
    }
    while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
    {
        fscanf(pFile_,"%d", &int_storage);
        stones_[inputIndices[0]] = int_storage;
    }
    fclose(pFile_);
    /*----------------- Read is done for stones------------------*/
    memcpy(stones, stones_, sizeof(stones_));

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    printf("\n");
    printf("k: %d\n", k);

    return 0;
}
