#!/bin/bash
i=$1
basefile=`echo $i | sed 's/.c//g'`
icc -O3 -vec-threshold0 -Ob0 -std=c99 -vec-report=9 $i -o icc$basefile
less ${1/.c/.optrpt}
