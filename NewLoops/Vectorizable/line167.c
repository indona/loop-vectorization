void cg_rc_ext_line167(int n, double  b[], double  x[], double  r[], double  z[], double  p[], double  q[], int job)
{
    int i;
    for(i = 0; i < n; i++)
    {
        p[i] = z[i];
    }
}


int main()
{
    int n = 1000;
    double b[100] = {3, 100, 999}, x[100] = {55, 69}, r[100] = {44}, z[100] = {44, 120}, p[100] = {33, 180, 160, 10}, q[100] = {89};
    int job = 80;

    cg_rc_ext_line167(n, b, x, r, z, p, q, job);
    return 0;
}
