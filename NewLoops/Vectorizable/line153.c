void cg_rc_ext_line153(int n, double  b[], double  x[], double  r[], double  z[], double  p[], double  q[], int job)
{
    int i;
    static double rho;
    for(i = 0; i < n; i++)
    {
        rho = rho + r[i] * z[i];
    }
}

int main()
{
    int n = 2000;
    double b[100] = {3}, x[100] = {55}, r[100] = {85, 44}, z[100] = {44}, p[100] = {33}, q[100] = {89};
    int job = 10;

    cg_rc_ext_line153(n, b, x, r, z, p, q, job);
    return 0;
}
