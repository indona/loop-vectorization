void cg_rc_ext_line187(int n, double  b[], double  x[], double  r[], double  z[], double  p[], double  q[], int job)
{
    int i;
    double alpha;
    for(i = 0; i < n; i++)
    {
        x[i] = x[i] + alpha * p[i];
    }
}


int main()
{
    int n = 1000;
    double b[100] = {3, 100}, x[100] = {69}, r[100] = {44}, z[100] = {44, 120}, p[100] = {160, 10}, q[100] = {89};
    int job=100;

    cg_rc_ext_line187(n, b, x, r, z, p, q, job);
    return 0;
}
