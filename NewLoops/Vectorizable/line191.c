void cg_rc_ext_line191(int n, double  b[], double  x[], double  r[], double  z[], double  p[], double  q[], int job)
{
    int i;
    double alpha;
    for(i = 0; i < n; i++)
    {
        r[i] = r[i] - alpha * q[i];
    }
}



int main()
{
    int n = 10000;
    double b[100] = {100, 969}, x[100] = {55}, r[100] = {44, 11, 900}, z[100] = {120}, p[100] = {180, 160}, q[100] = {89, 50};
    int job = 800;

    cg_rc_ext_line191(n, b, x, r, z, p, q, job);
    return 0;
}
