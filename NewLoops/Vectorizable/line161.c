void cg_rc_ext_line161(int n, double  b[], double  x[], double  r[], double  z[], double  p[], double  q[], int job)
{
    int i;
    double beta;
    for(i = 0; i < n; i++)
    {
        z[i] = z[i] + beta * p[i];
    }
}

int main()
{
    int n = 5000;
    double b[100] = {3}, x[100] = {55, 69, 100}, r[100] = {85, 44}, z[100] = {44}, p[100] = {33}, q[100] = {89, 50};
    int job = 8;

    cg_rc_ext_line161(n, b, x, r, z, p, q, job);
    return 0;
}

