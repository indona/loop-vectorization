#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "../dump/apsi.c_MAIN___line276_1.h"

extern void timer_clear(int n);
extern void timer_start(int n);
extern void timer_stop(int n);
extern double timer_read(int n);


typedef int integer;
integer i__;
typedef double doublereal;
doublereal  work[25000000] = {0};

void loop()
{
#pragma scop

    for(i__ = 1; i__ <= 25000000; ++i__)
    {
        work[i__ - 1] = 0.;
    }

#pragma endscop
}

int main()
{
    int inputIndices[10];
    FILE* pFile_;
    int work_dimension_sizes[1];

    timer_clear(1);
    timer_start(1);
    loop();

    timer_stop(1);
    double extracted_loop_exe_time = timer_read(1);
    printf("Simulation time: %lf seconds\n", extracted_loop_exe_time);

    unsigned char MD5_work[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_work;
    MD5_Init (&mdContext_work);
    for (int i0 = 0; i0 < 25000000; i0++)
    {
        MD5_Update(&mdContext_work, &work[i0], sizeof(double));
    }
    MD5_Final(MD5_work, &mdContext_work);
    printf("MD5_work: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_work[i]);
    }
    printf("\n");

    printf("\n");
    printf("i__: %d\n", i__);

    return 0;
}
