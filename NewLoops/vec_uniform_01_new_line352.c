void r8vec_uniform_01_new_ext_line352(int n, int * seed)
{
    int i;
    int k;
    int i4_huge = 2147483647;
    double *r;
    int cd=*seed;

    /*
    if(cd>0){
        cd *= (-2836)/127773;
        cd += i4_huge;
        r[i] =((double )( cd)) * 4.656612875E-10;
    }
    else{
        cd *= (-2836)/127773;
        r[i] =((double )( cd)) * 4.656612875E-10;
    }
    */

    r[0]=3;
    for(i = 1; i < n; i++)
    {
        //k=cd%127773;
        cd = 16807 * r[i-1];

        
        //cd = 16807*(cd%127773) - cd * (2836/127773);

        //cd = 16807 * (cd - (cd/127773)*127773) - cd * 2836;
        //if(cd < 0)
//            cd += i4_huge;
        r[i] =((double )( cd)) * 4.656612875E-10;
    }
    /*
    for(i = 0; i < n; i++);
    {
        k = *seed / 127773;
        *seed = 16807 *( *seed - k * 127773) - k * 2836;
        if( *seed < 0)
        {
            *seed = *seed + i4_huge;
        }
        r[i] =((double )( *seed)) * 4.656612875E-10;
    }
    */
}

int main()
{
    int n = 7000, m = 100;
    int * seed = &m;

    r8vec_uniform_01_new_ext_line352(n, seed);
    return 0;
}
