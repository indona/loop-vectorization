#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <inttypes.h>
#include <openssl/md5.h>
#include <sp.c_exact_rhs_line301_1.h>
#include <common.h>
#include <timer.h>

extern void* mallocMemoryChunk(char* fileName, void* startAddr, void* endAddr, unsigned  long long size);
extern void resetHeapData(void* startAddr, void* endAddr, unsigned  long long size);
extern void deleteMemoryMapping(void* oldBrk, void *addr, unsigned long long length);
extern void* saveCurrentStackToHeap(void*, unsigned long long*);
extern void* loadBenchStackToCurrentStack(char *stackDataFile, void* benchStackPtr, unsigned long long size);
extern void* getSavedAddress(void *heapAddr);
extern void restoreCurrentStack(void* heapAddr, unsigned long long size);

extern void loop();
extern void* dummy(int num, ...);

int m;
int i;
int  grid_points[3] = {0};
int j;
int k;
double  forcing[5][64 / 2 * 2 + 1][64 / 2 * 2 + 1][64 / 2 * 2 + 1] = {0};

int main(int argc, char** argv)
{
    int inputIndices[10];
    FILE* pFile_;
    int iterations = 1;
    void *oldBrk;

    static uint64_t tsc_start = 0, tsc_end, loop_cycles = 0;
    static uint64_t min_tsc_cycle = LLONG_MAX;

    if (argc < 3)
    {
        printf("Usage: %s path_to_data_files iterations\n", argv[0]);
        exit(1);
    }

    iterations = atoi(argv[2]);

    for (int it = 0; it < iterations; it++)
    {
        int forcing_dimension_sizes[4];
        memcpy(&k, &k_, sizeof(k_));
        memcpy(&j, &j_, sizeof(j_));

        /*----------------- read data for grid_points----------------------*/
        char grid_points_dataFileName[] = "/dataFile/sp.c_exact_rhs_line301_1.grid_points.dat";
        char grid_points_dataFileWithPath[256];
        sprintf (grid_points_dataFileWithPath, "%s/%s", argv[1], grid_points_dataFileName);
        int int_storage;

        if ( it < 1 )
        {
            pFile_ = fopen(grid_points_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line301_1.grid_points.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_,"%d", &int_storage);
                grid_points_[inputIndices[0]] = int_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for grid_points------------------*/
        memcpy(grid_points, grid_points_, sizeof(grid_points_));
        memcpy(&i, &i_, sizeof(i_));
        memcpy(&m, &m_, sizeof(m_));

        tsc_start = rdtsc();

        loop();

        tsc_end = rdtsc();

        loop_cycles = tsc_end - tsc_start;
        if (loop_cycles < min_tsc_cycle)
            min_tsc_cycle = loop_cycles;

    }

    printf("Simulation time: %" PRIu64 " cycles\n", min_tsc_cycle);

    unsigned char MD5_forcing[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_forcing;
    MD5_Init (&mdContext_forcing);
    for (int i0 = 0; i0 < 5; i0++)
    {
        for (int i1 = 0; i1 < 64 / 2 * 2 + 1; i1++)
        {
            for (int i2 = 0; i2 < 64 / 2 * 2 + 1; i2++)
            {
                for (int i3 = 0; i3 < 64 / 2 * 2 + 1; i3++)
                {
                    MD5_Update(&mdContext_forcing, &forcing[i0][i1][i2][i3], sizeof(double));
                }
            }
        }
    }
    MD5_Final(MD5_forcing, &mdContext_forcing);
    printf("MD5_forcing: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_forcing[i]);
    }
    printf("\n");

    printf("\n");
    printf("k: %d\n", k);
    printf("j: %d\n", j);
    printf("i: %d\n", i);
    printf("m: %d\n", m);


    return 0;
}
