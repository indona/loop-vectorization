#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <inttypes.h>
#include <openssl/md5.h>
#include <3mm.c_kernel_3mm_line101_1.h>
#include <common.h>
#include <timer.h>

extern void* mallocMemoryChunk(char* fileName, void* startAddr, void* endAddr, unsigned  long long size);
extern void resetHeapData(void* startAddr, void* endAddr, unsigned  long long size);
extern void deleteMemoryMapping(void* oldBrk, void *addr, unsigned long long length);
extern void* saveCurrentStackToHeap(void*, unsigned long long*);
extern void* loadBenchStackToCurrentStack(char *stackDataFile, void* benchStackPtr, unsigned long long size);
extern void* getSavedAddress(void *heapAddr);
extern void restoreCurrentStack(void* heapAddr, unsigned long long size);

extern void loop();
extern void* dummy(int num, ...);

int i;
int ni;
int j;
int nl;
double  G[800 + 0][1100 + 0] = {0};
int k;
int nj;
double  E[800 + 0][900 + 0] = {0};
double  F[900 + 0][1100 + 0] = {0};

int main(int argc, char** argv)
{
    int inputIndices[10];
    FILE* pFile_;
    int iterations = 1;
    void *oldBrk;

    static uint64_t tsc_start = 0, tsc_end, loop_cycles = 0;
    static uint64_t min_tsc_cycle = LLONG_MAX;

    if (argc < 3)
    {
        printf("Usage: %s path_to_data_files iterations\n", argv[0]);
        exit(1);
    }

    iterations = atoi(argv[2]);

    for (int it = 0; it < iterations; it++)
    {

        /*----------------- read data for F----------------------*/
        char F_dataFileName[] = "/dataFile/3mm.c_kernel_3mm_line101_1.F.dat";
        char F_dataFileWithPath[256];
        sprintf (F_dataFileWithPath, "%s/%s", argv[1], F_dataFileName);
        double double_storage;

        if ( it < 1 )
        {
            pFile_ = fopen(F_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/3mm.c_kernel_3mm_line101_1.F.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_, "%d", &inputIndices[1]);
                fscanf(pFile_,"%lf", &double_storage);
                F_[inputIndices[0]][inputIndices[1]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for F------------------*/
        memcpy(F, F_, sizeof(F_));

        /*----------------- read data for E----------------------*/
        char E_dataFileName[] = "/dataFile/3mm.c_kernel_3mm_line101_1.E.dat";
        char E_dataFileWithPath[256];
        sprintf (E_dataFileWithPath, "%s/%s", argv[1], E_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(E_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/3mm.c_kernel_3mm_line101_1.E.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_, "%d", &inputIndices[1]);
                fscanf(pFile_,"%lf", &double_storage);
                E_[inputIndices[0]][inputIndices[1]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for E------------------*/
        memcpy(E, E_, sizeof(E_));
        memcpy(&nj, &nj_, sizeof(nj_));
        memcpy(&k, &k_, sizeof(k_));
        int G_dimension_sizes[2];

        /*----------------- read data for G----------------------*/
        char G_dataFileName[] = "/dataFile/3mm.c_kernel_3mm_line101_1.G.dat";
        char G_dataFileWithPath[256];
        sprintf (G_dataFileWithPath, "%s/%s", argv[1], G_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(G_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/3mm.c_kernel_3mm_line101_1.G.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_, "%d", &inputIndices[1]);
                fscanf(pFile_,"%lf", &double_storage);
                G_[inputIndices[0]][inputIndices[1]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for G------------------*/
        memcpy(G, G_, sizeof(G_));
        memcpy(&nl, &nl_, sizeof(nl_));
        memcpy(&j, &j_, sizeof(j_));
        memcpy(&ni, &ni_, sizeof(ni_));
        memcpy(&i, &i_, sizeof(i_));

        tsc_start = rdtsc();

        loop();

        tsc_end = rdtsc();

        loop_cycles = tsc_end - tsc_start;
        if (loop_cycles < min_tsc_cycle)
            min_tsc_cycle = loop_cycles;

    }

    printf("Simulation time: %" PRIu64 " cycles\n", min_tsc_cycle);

    unsigned char MD5_G[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_G;
    MD5_Init (&mdContext_G);
    for (int i0 = 0; i0 < 800 + 0; i0++)
    {
        for (int i1 = 0; i1 < 1100 + 0; i1++)
        {
            MD5_Update(&mdContext_G, &G[i0][i1], sizeof(double));
        }
    }
    MD5_Final(MD5_G, &mdContext_G);
    printf("MD5_G: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_G[i]);
    }
    printf("\n");

    printf("\n");
    printf("k: %d\n", k);
    printf("j: %d\n", j);
    printf("i: %d\n", i);


    return 0;
}
