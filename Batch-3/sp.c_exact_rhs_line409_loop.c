#include <inttypes.h>
extern int k;
extern int  grid_points[3] ;
extern double zeta;
extern double dnzm1;
extern int i;
extern double xi;
extern double dnxm1;
extern int j;
extern double eta;
extern double dnym1;
extern double  dtemp[5] ;
extern double  ce[13][5] ;
extern double  ue[5][64] ;
extern double dtpp;
extern double  buf[5][64] ;
extern double  cuf[64] ;
extern double  q[64] ;
extern int jm1;
extern int jp1;
extern double  forcing[5][64 / 2 * 2 + 1][64 / 2 * 2 + 1][64 / 2 * 2 + 1] ;
extern double ty2;
extern double dy1ty1;
extern double yycon2;
extern double dy2ty1;
extern double c2;
extern double yycon1;
extern double dy3ty1;
extern double dy4ty1;
extern double c1;
extern double yycon3;
extern double yycon4;
extern double yycon5;
extern double dy5ty1;
extern double dssp;

void loop()
{
#pragma scop

    for(k = 1; k <= grid_points[2] - 2; k++)
    {
        zeta =((double )k) * dnzm1;
        for(i = 1; i <= grid_points[0] - 2; i++)
        {
            int m;
            xi =((double )i) * dnxm1;
            for(j = 0; j <= grid_points[1] - 1; j++)
            {
                eta =((double )j) * dnym1;
                {
                    for(m = 0; m < 5; m++)
                    {
                        *(dtemp + m) = ce[0][m] + xi *(ce[1][m] + xi *(ce[4][m] + xi *(ce[7][m] + xi * ce['\n'][m]))) + eta *(ce[2][m] + eta *(ce[5][m] + eta *(ce[8][m] + eta * ce[11][m]))) + zeta *(ce[3][m] + zeta *(ce[6][m] + zeta *(ce[9][m] + zeta * ce[12][m])));
                    }
                };
                for(m = 0; m < 5; m++)
                {
                    ue[m][j] = dtemp[m];
                }
                dtpp = 1.0 / dtemp[0];
                for(m = 1; m < 5; m++)
                {
                    buf[m][j] = dtpp * dtemp[m];
                }
                cuf[j] = buf[2][j] * buf[2][j];
                buf[0][j] = cuf[j] + buf[1][j] * buf[1][j] + buf[3][j] * buf[3][j];
                q[j] = 0.5 *(buf[1][j] * ue[1][j] + buf[2][j] * ue[2][j] + buf[3][j] * ue[3][j]);
            }
            for(j = 1; j <= grid_points[1] - 2; j++)
            {
                jm1 = j - 1;
                jp1 = j + 1;
                forcing[0][i][j][k] = forcing[0][i][j][k] - ty2 *(ue[2][jp1] - ue[2][jm1]) + dy1ty1 *(ue[0][jp1] - 2.0 * ue[0][j] + ue[0][jm1]);
                forcing[1][i][j][k] = forcing[1][i][j][k] - ty2 *(ue[1][jp1] * buf[2][jp1] - ue[1][jm1] * buf[2][jm1]) + yycon2 *(buf[1][jp1] - 2.0 * buf[1][j] + buf[1][jm1]) + dy2ty1 *(ue[1][jp1] - 2.0 * ue[1][j] + ue[1][jm1]);
                forcing[2][i][j][k] = forcing[2][i][j][k] - ty2 *(ue[2][jp1] * buf[2][jp1] + c2 *(ue[4][jp1] - q[jp1]) -(ue[2][jm1] * buf[2][jm1] + c2 *(ue[4][jm1] - q[jm1]))) + yycon1 *(buf[2][jp1] - 2.0 * buf[2][j] + buf[2][jm1]) + dy3ty1 *(ue[2][jp1] - 2.0 * ue[2][j] + ue[2][jm1]);
                forcing[3][i][j][k] = forcing[3][i][j][k] - ty2 *(ue[3][jp1] * buf[2][jp1] - ue[3][jm1] * buf[2][jm1]) + yycon2 *(buf[3][jp1] - 2.0 * buf[3][j] + buf[3][jm1]) + dy4ty1 *(ue[3][jp1] - 2.0 * ue[3][j] + ue[3][jm1]);
                forcing[4][i][j][k] = forcing[4][i][j][k] - ty2 *(buf[2][jp1] *(c1 * ue[4][jp1] - c2 * q[jp1]) - buf[2][jm1] *(c1 * ue[4][jm1] - c2 * q[jm1])) + 0.5 * yycon3 *(buf[0][jp1] - 2.0 * buf[0][j] + buf[0][jm1]) + yycon4 *(cuf[jp1] - 2.0 * cuf[j] + cuf[jm1]) + yycon5 *(buf[4][jp1] - 2.0 * buf[4][j] + buf[4][jm1]) + dy5ty1 *(ue[4][jp1] - 2.0 * ue[4][j] + ue[4][jm1]);
            }
            for(m = 0; m < 5; m++)
            {
                j = 1;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(5.0 * ue[m][j] - 4.0 * ue[m][j + 1] + ue[m][j + 2]);
                j = 2;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(- 4.0 * ue[m][j - 1] + 6.0 * ue[m][j] - 4.0 * ue[m][j + 1] + ue[m][j + 2]);
            }
            for(m = 0; m < 5; m++)
            {
                for(j = 3; j <= grid_points[1] - 4; j++)
                {
                    forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(ue[m][j - 2] - 4.0 * ue[m][j - 1] + 6.0 * ue[m][j] - 4.0 * ue[m][j + 1] + ue[m][j + 2]);
                }
            }
            for(m = 0; m < 5; m++)
            {
                j = grid_points[1] - 3;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(ue[m][j - 2] - 4.0 * ue[m][j - 1] + 6.0 * ue[m][j] - 4.0 * ue[m][j + 1]);
                j = grid_points[1] - 2;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(ue[m][j - 2] - 4.0 * ue[m][j - 1] + 5.0 * ue[m][j]);
            }
        }
    }

#pragma endscop
}
