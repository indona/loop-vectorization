#include <inttypes.h>
extern int k;
extern int  grid_points[3] ;
extern double zeta;
extern double dnzm1;
extern int j;
extern double eta;
extern double dnym1;
extern int i;
extern double xi;
extern double dnxm1;
extern double  dtemp[5] ;
extern double  ce[13][5] ;
extern double  ue[5][64] ;
extern double dtpp;
extern double  buf[5][64] ;
extern double  cuf[64] ;
extern double  q[64] ;
extern int im1;
extern int ip1;
extern double  forcing[5][64 / 2 * 2 + 1][64 / 2 * 2 + 1][64 / 2 * 2 + 1] ;
extern double tx2;
extern double dx1tx1;
extern double c2;
extern double xxcon1;
extern double dx2tx1;
extern double xxcon2;
extern double dx3tx1;
extern double dx4tx1;
extern double c1;
extern double xxcon3;
extern double xxcon4;
extern double xxcon5;
extern double dx5tx1;
extern double dssp;

void loop()
{
#pragma scop

    for(k = 1; k <= grid_points[2] - 2; k++)
    {
        zeta =((double )k) * dnzm1;
        for(j = 1; j <= grid_points[1] - 2; j++)
        {
            int m;
            eta =((double )j) * dnym1;
            for(i = 0; i <= grid_points[0] - 1; i++)
            {
                xi =((double )i) * dnxm1;
                {
                    for(m = 0; m < 5; m++)
                    {
                        *(dtemp + m) = ce[0][m] + xi *(ce[1][m] + xi *(ce[4][m] + xi *(ce[7][m] + xi * ce['\n'][m]))) + eta *(ce[2][m] + eta *(ce[5][m] + eta *(ce[8][m] + eta * ce[11][m]))) + zeta *(ce[3][m] + zeta *(ce[6][m] + zeta *(ce[9][m] + zeta * ce[12][m])));
                    }
                };
                for(m = 0; m < 5; m++)
                {
                    ue[m][i] = dtemp[m];
                }
                dtpp = 1.0 / dtemp[0];
                for(m = 1; m < 5; m++)
                {
                    buf[m][i] = dtpp * dtemp[m];
                }
                cuf[i] = buf[1][i] * buf[1][i];
                buf[0][i] = cuf[i] + buf[2][i] * buf[2][i] + buf[3][i] * buf[3][i];
                q[i] = 0.5 *(buf[1][i] * ue[1][i] + buf[2][i] * ue[2][i] + buf[3][i] * ue[3][i]);
            }
            for(i = 1; i <= grid_points[0] - 2; i++)
            {
                im1 = i - 1;
                ip1 = i + 1;
                forcing[0][i][j][k] = forcing[0][i][j][k] - tx2 *(ue[1][ip1] - ue[1][im1]) + dx1tx1 *(ue[0][ip1] - 2.0 * ue[0][i] + ue[0][im1]);
                forcing[1][i][j][k] = forcing[1][i][j][k] - tx2 *(ue[1][ip1] * buf[1][ip1] + c2 *(ue[4][ip1] - q[ip1]) -(ue[1][im1] * buf[1][im1] + c2 *(ue[4][im1] - q[im1]))) + xxcon1 *(buf[1][ip1] - 2.0 * buf[1][i] + buf[1][im1]) + dx2tx1 *(ue[1][ip1] - 2.0 * ue[1][i] + ue[1][im1]);
                forcing[2][i][j][k] = forcing[2][i][j][k] - tx2 *(ue[2][ip1] * buf[1][ip1] - ue[2][im1] * buf[1][im1]) + xxcon2 *(buf[2][ip1] - 2.0 * buf[2][i] + buf[2][im1]) + dx3tx1 *(ue[2][ip1] - 2.0 * ue[2][i] + ue[2][im1]);
                forcing[3][i][j][k] = forcing[3][i][j][k] - tx2 *(ue[3][ip1] * buf[1][ip1] - ue[3][im1] * buf[1][im1]) + xxcon2 *(buf[3][ip1] - 2.0 * buf[3][i] + buf[3][im1]) + dx4tx1 *(ue[3][ip1] - 2.0 * ue[3][i] + ue[3][im1]);
                forcing[4][i][j][k] = forcing[4][i][j][k] - tx2 *(buf[1][ip1] *(c1 * ue[4][ip1] - c2 * q[ip1]) - buf[1][im1] *(c1 * ue[4][im1] - c2 * q[im1])) + 0.5 * xxcon3 *(buf[0][ip1] - 2.0 * buf[0][i] + buf[0][im1]) + xxcon4 *(cuf[ip1] - 2.0 * cuf[i] + cuf[im1]) + xxcon5 *(buf[4][ip1] - 2.0 * buf[4][i] + buf[4][im1]) + dx5tx1 *(ue[4][ip1] - 2.0 * ue[4][i] + ue[4][im1]);
            }
            for(m = 0; m < 5; m++)
            {
                i = 1;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(5.0 * ue[m][i] - 4.0 * ue[m][i + 1] + ue[m][i + 2]);
                i = 2;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(- 4.0 * ue[m][i - 1] + 6.0 * ue[m][i] - 4.0 * ue[m][i + 1] + ue[m][i + 2]);
            }
            for(m = 0; m < 5; m++)
            {
                for(i = 3; i <= grid_points[0] - 4; i++)
                {
                    forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(ue[m][i - 2] - 4.0 * ue[m][i - 1] + 6.0 * ue[m][i] - 4.0 * ue[m][i + 1] + ue[m][i + 2]);
                }
            }
            for(m = 0; m < 5; m++)
            {
                i = grid_points[0] - 3;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(ue[m][i - 2] - 4.0 * ue[m][i - 1] + 6.0 * ue[m][i] - 4.0 * ue[m][i + 1]);
                i = grid_points[0] - 2;
                forcing[m][i][j][k] = forcing[m][i][j][k] - dssp *(ue[m][i - 2] - 4.0 * ue[m][i - 1] + 5.0 * ue[m][i]);
            }
        }
    }

#pragma endscop
}
