#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <inttypes.h>
#include <openssl/md5.h>
#include <sp.c_exact_rhs_line314_1.h>
#include <common.h>
#include <timer.h>

extern void* mallocMemoryChunk(char* fileName, void* startAddr, void* endAddr, unsigned  long long size);
extern void resetHeapData(void* startAddr, void* endAddr, unsigned  long long size);
extern void deleteMemoryMapping(void* oldBrk, void *addr, unsigned long long length);
extern void* saveCurrentStackToHeap(void*, unsigned long long*);
extern void* loadBenchStackToCurrentStack(char *stackDataFile, void* benchStackPtr, unsigned long long size);
extern void* getSavedAddress(void *heapAddr);
extern void restoreCurrentStack(void* heapAddr, unsigned long long size);

extern void loop();
extern void* dummy(int num, ...);

int k;
int  grid_points[3] = {0};
double zeta;
double dnzm1;
int j;
double eta;
double dnym1;
int i;
double xi;
double dnxm1;
double  dtemp[5] = {0};
double  ce[13][5] = {0};
double  ue[5][64] = {0};
double dtpp;
double  buf[5][64] = {0};
double  cuf[64] = {0};
double  q[64] = {0};
int im1;
int ip1;
double  forcing[5][64 / 2 * 2 + 1][64 / 2 * 2 + 1][64 / 2 * 2 + 1] = {0};
double tx2;
double dx1tx1;
double c2;
double xxcon1;
double dx2tx1;
double xxcon2;
double dx3tx1;
double dx4tx1;
double c1;
double xxcon3;
double xxcon4;
double xxcon5;
double dx5tx1;
double dssp;

int main(int argc, char** argv)
{
    int inputIndices[10];
    FILE* pFile_;
    int iterations = 1;
    void *oldBrk;

    static uint64_t tsc_start = 0, tsc_end, loop_cycles = 0;
    static uint64_t min_tsc_cycle = LLONG_MAX;

    if (argc < 3)
    {
        printf("Usage: %s path_to_data_files iterations\n", argv[0]);
        exit(1);
    }

    iterations = atoi(argv[2]);

    for (int it = 0; it < iterations; it++)
    {
        memcpy(&dssp, &dssp_, sizeof(dssp_));
        memcpy(&dx5tx1, &dx5tx1_, sizeof(dx5tx1_));
        memcpy(&xxcon5, &xxcon5_, sizeof(xxcon5_));
        memcpy(&xxcon4, &xxcon4_, sizeof(xxcon4_));
        memcpy(&xxcon3, &xxcon3_, sizeof(xxcon3_));
        memcpy(&c1, &c1_, sizeof(c1_));
        memcpy(&dx4tx1, &dx4tx1_, sizeof(dx4tx1_));
        memcpy(&dx3tx1, &dx3tx1_, sizeof(dx3tx1_));
        memcpy(&xxcon2, &xxcon2_, sizeof(xxcon2_));
        memcpy(&dx2tx1, &dx2tx1_, sizeof(dx2tx1_));
        memcpy(&xxcon1, &xxcon1_, sizeof(xxcon1_));
        memcpy(&c2, &c2_, sizeof(c2_));
        memcpy(&dx1tx1, &dx1tx1_, sizeof(dx1tx1_));
        memcpy(&tx2, &tx2_, sizeof(tx2_));
        int forcing_dimension_sizes[4];

        /*----------------- read data for forcing----------------------*/
        char forcing_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.forcing.dat";
        char forcing_dataFileWithPath[256];
        sprintf (forcing_dataFileWithPath, "%s/%s", argv[1], forcing_dataFileName);
        double double_storage;

        if ( it < 1 )
        {
            pFile_ = fopen(forcing_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.forcing.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_, "%d", &inputIndices[1]);
                fscanf(pFile_, "%d", &inputIndices[2]);
                fscanf(pFile_, "%d", &inputIndices[3]);
                fscanf(pFile_,"%lf", &double_storage);
                forcing_[inputIndices[0]][inputIndices[1]][inputIndices[2]][inputIndices[3]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for forcing------------------*/
        memcpy(forcing, forcing_, sizeof(forcing_));
        memcpy(&ip1, &ip1_, sizeof(ip1_));
        memcpy(&im1, &im1_, sizeof(im1_));
        int q_dimension_sizes[1];

        /*----------------- read data for q----------------------*/
        char q_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.q.dat";
        char q_dataFileWithPath[256];
        sprintf (q_dataFileWithPath, "%s/%s", argv[1], q_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(q_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.q.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_,"%lf", &double_storage);
                q_[inputIndices[0]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for q------------------*/
        memcpy(q, q_, sizeof(q_));
        int cuf_dimension_sizes[1];

        /*----------------- read data for cuf----------------------*/
        char cuf_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.cuf.dat";
        char cuf_dataFileWithPath[256];
        sprintf (cuf_dataFileWithPath, "%s/%s", argv[1], cuf_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(cuf_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.cuf.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_,"%lf", &double_storage);
                cuf_[inputIndices[0]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for cuf------------------*/
        memcpy(cuf, cuf_, sizeof(cuf_));
        int buf_dimension_sizes[2];

        /*----------------- read data for buf----------------------*/
        char buf_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.buf.dat";
        char buf_dataFileWithPath[256];
        sprintf (buf_dataFileWithPath, "%s/%s", argv[1], buf_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(buf_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.buf.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_, "%d", &inputIndices[1]);
                fscanf(pFile_,"%lf", &double_storage);
                buf_[inputIndices[0]][inputIndices[1]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for buf------------------*/
        memcpy(buf, buf_, sizeof(buf_));
        memcpy(&dtpp, &dtpp_, sizeof(dtpp_));
        int ue_dimension_sizes[2];

        /*----------------- read data for ue----------------------*/
        char ue_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.ue.dat";
        char ue_dataFileWithPath[256];
        sprintf (ue_dataFileWithPath, "%s/%s", argv[1], ue_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(ue_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.ue.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_, "%d", &inputIndices[1]);
                fscanf(pFile_,"%lf", &double_storage);
                ue_[inputIndices[0]][inputIndices[1]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for ue------------------*/
        memcpy(ue, ue_, sizeof(ue_));

        /*----------------- read data for ce----------------------*/
        char ce_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.ce.dat";
        char ce_dataFileWithPath[256];
        sprintf (ce_dataFileWithPath, "%s/%s", argv[1], ce_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(ce_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.ce.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_, "%d", &inputIndices[1]);
                fscanf(pFile_,"%lf", &double_storage);
                ce_[inputIndices[0]][inputIndices[1]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for ce------------------*/
        memcpy(ce, ce_, sizeof(ce_));

        /*----------------- read data for dtemp----------------------*/
        char dtemp_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.dtemp.dat";
        char dtemp_dataFileWithPath[256];
        sprintf (dtemp_dataFileWithPath, "%s/%s", argv[1], dtemp_dataFileName);

        if ( it < 1 )
        {
            pFile_ = fopen(dtemp_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.dtemp.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_,"%lf", &double_storage);
                dtemp_[inputIndices[0]] = double_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for dtemp------------------*/
        memcpy(dtemp, dtemp_, sizeof(dtemp_));
        memcpy(&dnxm1, &dnxm1_, sizeof(dnxm1_));
        memcpy(&xi, &xi_, sizeof(xi_));
        memcpy(&i, &i_, sizeof(i_));
        memcpy(&dnym1, &dnym1_, sizeof(dnym1_));
        memcpy(&eta, &eta_, sizeof(eta_));
        memcpy(&j, &j_, sizeof(j_));
        memcpy(&dnzm1, &dnzm1_, sizeof(dnzm1_));
        memcpy(&zeta, &zeta_, sizeof(zeta_));

        /*----------------- read data for grid_points----------------------*/
        char grid_points_dataFileName[] = "/dataFile/sp.c_exact_rhs_line314_1.grid_points.dat";
        char grid_points_dataFileWithPath[256];
        sprintf (grid_points_dataFileWithPath, "%s/%s", argv[1], grid_points_dataFileName);
        int int_storage;

        if ( it < 1 )
        {
            pFile_ = fopen(grid_points_dataFileWithPath, "r");
            if (pFile_ == NULL)
            {
                printf("data file: /dataFile/sp.c_exact_rhs_line314_1.grid_points.dat does not exist!");
                exit(-1);
            }
            while(fscanf(pFile_, "%d", &inputIndices[0]) != EOF)
            {
                fscanf(pFile_,"%d", &int_storage);
                grid_points_[inputIndices[0]] = int_storage;
            }
            fclose(pFile_);
        }
        /*----------------- Read is done for grid_points------------------*/
        memcpy(grid_points, grid_points_, sizeof(grid_points_));
        memcpy(&k, &k_, sizeof(k_));

        tsc_start = rdtsc();

        loop();

        tsc_end = rdtsc();

        loop_cycles = tsc_end - tsc_start;
        if (loop_cycles < min_tsc_cycle)
            min_tsc_cycle = loop_cycles;

    }

    printf("Simulation time: %" PRIu64 " cycles\n", min_tsc_cycle);

    unsigned char MD5_forcing[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_forcing;
    MD5_Init (&mdContext_forcing);
    for (int i0 = 0; i0 < 5; i0++)
    {
        for (int i1 = 0; i1 < 64 / 2 * 2 + 1; i1++)
        {
            for (int i2 = 0; i2 < 64 / 2 * 2 + 1; i2++)
            {
                for (int i3 = 0; i3 < 64 / 2 * 2 + 1; i3++)
                {
                    MD5_Update(&mdContext_forcing, &forcing[i0][i1][i2][i3], sizeof(double));
                }
            }
        }
    }
    MD5_Final(MD5_forcing, &mdContext_forcing);
    printf("MD5_forcing: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_forcing[i]);
    }
    printf("\n");

    unsigned char MD5_q[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_q;
    MD5_Init (&mdContext_q);
    for (int i0 = 0; i0 < 64; i0++)
    {
        MD5_Update(&mdContext_q, &q[i0], sizeof(double));
    }
    MD5_Final(MD5_q, &mdContext_q);
    printf("MD5_q: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_q[i]);
    }
    printf("\n");

    unsigned char MD5_cuf[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_cuf;
    MD5_Init (&mdContext_cuf);
    for (int i0 = 0; i0 < 64; i0++)
    {
        MD5_Update(&mdContext_cuf, &cuf[i0], sizeof(double));
    }
    MD5_Final(MD5_cuf, &mdContext_cuf);
    printf("MD5_cuf: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_cuf[i]);
    }
    printf("\n");

    unsigned char MD5_buf[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_buf;
    MD5_Init (&mdContext_buf);
    for (int i0 = 0; i0 < 5; i0++)
    {
        for (int i1 = 0; i1 < 64; i1++)
        {
            MD5_Update(&mdContext_buf, &buf[i0][i1], sizeof(double));
        }
    }
    MD5_Final(MD5_buf, &mdContext_buf);
    printf("MD5_buf: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_buf[i]);
    }
    printf("\n");

    unsigned char MD5_ue[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_ue;
    MD5_Init (&mdContext_ue);
    for (int i0 = 0; i0 < 5; i0++)
    {
        for (int i1 = 0; i1 < 64; i1++)
        {
            MD5_Update(&mdContext_ue, &ue[i0][i1], sizeof(double));
        }
    }
    MD5_Final(MD5_ue, &mdContext_ue);
    printf("MD5_ue: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_ue[i]);
    }
    printf("\n");

    printf("\n");
    printf("ip1: %d\n", ip1);
    printf("im1: %d\n", im1);
    printf("dtpp: %lf\n", dtpp);
    printf("xi: %lf\n", xi);
    printf("i: %d\n", i);
    printf("eta: %lf\n", eta);
    printf("j: %d\n", j);
    printf("zeta: %lf\n", zeta);
    printf("k: %d\n", k);


    return 0;
}
