#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <inttypes.h>
#include <openssl/md5.h>
#include <3mm.c_init_array_line43_1.h>
#include <common.h>
#include <timer.h>

extern void* mallocMemoryChunk(char* fileName, void* startAddr, void* endAddr, unsigned  long long size);
extern void resetHeapData(void* startAddr, void* endAddr, unsigned  long long size);
extern void deleteMemoryMapping(void* oldBrk, void *addr, unsigned long long length);
extern void* saveCurrentStackToHeap(void*, unsigned long long*);
extern void* loadBenchStackToCurrentStack(char *stackDataFile, void* benchStackPtr, unsigned long long size);
extern void* getSavedAddress(void *heapAddr);
extern void restoreCurrentStack(void* heapAddr, unsigned long long size);

extern void loop();
extern void* dummy(int num, ...);

int i;
int nm;
int j;
int nl;
double  D[1200 + 0][1100 + 0] = {0};
int nk;

int main(int argc, char** argv)
{
    int inputIndices[10];
    FILE* pFile_;
    int iterations = 1;
    void *oldBrk;

    static uint64_t tsc_start = 0, tsc_end, loop_cycles = 0;
    static uint64_t min_tsc_cycle = LLONG_MAX;

    if (argc < 3)
    {
        printf("Usage: %s path_to_data_files iterations\n", argv[0]);
        exit(1);
    }

    iterations = atoi(argv[2]);

    for (int it = 0; it < iterations; it++)
    {
        memcpy(&nk, &nk_, sizeof(nk_));
        int D_dimension_sizes[2];
        memcpy(&nl, &nl_, sizeof(nl_));
        memcpy(&j, &j_, sizeof(j_));
        memcpy(&nm, &nm_, sizeof(nm_));
        memcpy(&i, &i_, sizeof(i_));

        tsc_start = rdtsc();

        loop();

        tsc_end = rdtsc();

        loop_cycles = tsc_end - tsc_start;
        if (loop_cycles < min_tsc_cycle)
            min_tsc_cycle = loop_cycles;

    }

    printf("Simulation time: %" PRIu64 " cycles\n", min_tsc_cycle);

    unsigned char MD5_D[MD5_DIGEST_LENGTH];
    MD5_CTX mdContext_D;
    MD5_Init (&mdContext_D);
    for (int i0 = 0; i0 < 1200 + 0; i0++)
    {
        for (int i1 = 0; i1 < 1100 + 0; i1++)
        {
            MD5_Update(&mdContext_D, &D[i0][i1], sizeof(double));
        }
    }
    MD5_Final(MD5_D, &mdContext_D);
    printf("MD5_D: ");
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        printf("%02x", MD5_D[i]);
    }
    printf("\n");

    printf("\n");
    printf("j: %d\n", j);
    printf("i: %d\n", i);


    return 0;
}
