#include <rmatmult3.c_rmatmult3_line79_1.ptrderefout.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <inttypes.h>
#include <openssl/md5.h>
#include <rmatmult3.c_rmatmult3_line79_1.h>
#include <common.h>
#include <timer.h>

extern void* mallocMemoryChunk(char* fileName, void* startAddr, void* endAddr, unsigned  long long size);
extern void resetHeapData(void* startAddr, void* endAddr, unsigned  long long size);
extern void deleteMemoryMapping(void* oldBrk, void *addr, unsigned long long length);
extern void* saveCurrentStackToHeap(void*, unsigned long long*);
extern void* loadBenchStackToCurrentStack(char *stackDataFile, void* benchStackPtr, unsigned long long size);
extern void* getSavedAddress(void *heapAddr);
extern void restoreCurrentStack(void* heapAddr, unsigned long long size);

extern void loop();
extern void* dummy(int num, ...);

int kk;
int kmin;
int kmax;
int jj;
int jmin;
int jmax;
int ii;
int imin;
int imax;
int i;
int jp;
int kp;
double * restrict b;
double * restrict dbl;
double * restrict xdbl;
double * restrict dbc;
double * restrict xdbc;
double * restrict dbr;
double * restrict xdbr;
double * restrict dcl;
double * restrict xdcl;
double * restrict dcc;
double * restrict xdcc;
double * restrict dcr;
double * restrict xdcr;
double * restrict dfl;
double * restrict xdfl;
double * restrict dfc;
double * restrict xdfc;
double * restrict dfr;
double * restrict xdfr;
double * restrict cbl;
double * restrict xcbl;
double * restrict cbc;
double * restrict xcbc;
double * restrict cbr;
double * restrict xcbr;
double * restrict ccl;
double * restrict xccl;
double * restrict ccc;
double * restrict xccc;
double * restrict ccr;
double * restrict xccr;
double * restrict cfl;
double * restrict xcfl;
double * restrict cfc;
double * restrict xcfc;
double * restrict cfr;
double * restrict xcfr;
double * restrict ubl;
double * restrict xubl;
double * restrict ubc;
double * restrict xubc;
double * restrict ubr;
double * restrict xubr;
double * restrict ucl;
double * restrict xucl;
double * restrict ucc;
double * restrict xucc;
double * restrict ucr;
double * restrict xucr;
double * restrict ufl;
double * restrict xufl;
double * restrict ufc;
double * restrict xufc;
double * restrict ufr;
double * restrict xufr;

int main(int argc, char** argv)
{
    int inputIndices[10];
    FILE* pFile_;
    int iterations = 1;
    void *oldBrk;

    static char* inputDataFileName[] = {"rmatmult3.c_rmatmult3_line79_1.ptrderefout.hd", "rmatmult3.c_rmatmult3_line79_1.ptrderefout.st", "rmatmult3.c_rmatmult3_line79_1.ptrderefout.gl"};
    static char heapDataFile[256] = {0};
    static char stackDataFile[256] = {0};
    static char globalDataFile[256] = {0};
    static uint64_t tsc_start = 0, tsc_end, loop_cycles = 0;
    static uint64_t min_tsc_cycle = LLONG_MAX;

    if (argc < 3)
    {
        printf("Usage: %s path_to_data_files iterations\n", argv[0]);
        exit(1);
    }

    sprintf (heapDataFile, "%s/%s", argv[1], inputDataFileName[0]);
    sprintf (stackDataFile, "%s/%s", argv[1], inputDataFileName[1]);
    sprintf (globalDataFile, "%s/%s", argv[1], inputDataFileName[2]);

    iterations = atoi(argv[2]);

    for (int it = 0; it < iterations; it++)
    {
        b = (double *) b_;
        dbl = (double *) dbl_;
        xdbl = (double *) xdbl_;
        dbc = (double *) dbc_;
        xdbc = (double *) xdbc_;
        dbr = (double *) dbr_;
        xdbr = (double *) xdbr_;
        dcl = (double *) dcl_;
        xdcl = (double *) xdcl_;
        dcc = (double *) dcc_;
        xdcc = (double *) xdcc_;
        dcr = (double *) dcr_;
        xdcr = (double *) xdcr_;
        dfl = (double *) dfl_;
        xdfl = (double *) xdfl_;
        dfc = (double *) dfc_;
        xdfc = (double *) xdfc_;
        dfr = (double *) dfr_;
        xdfr = (double *) xdfr_;
        cbl = (double *) cbl_;
        xcbl = (double *) xcbl_;
        cbc = (double *) cbc_;
        xcbc = (double *) xcbc_;
        cbr = (double *) cbr_;
        xcbr = (double *) xcbr_;
        ccl = (double *) ccl_;
        xccl = (double *) xccl_;
        ccc = (double *) ccc_;
        xccc = (double *) xccc_;
        ccr = (double *) ccr_;
        xccr = (double *) xccr_;
        cfl = (double *) cfl_;
        xcfl = (double *) xcfl_;
        cfc = (double *) cfc_;
        xcfc = (double *) xcfc_;
        cfr = (double *) cfr_;
        xcfr = (double *) xcfr_;
        ubl = (double *) ubl_;
        xubl = (double *) xubl_;
        ubc = (double *) ubc_;
        xubc = (double *) xubc_;
        ubr = (double *) ubr_;
        xubr = (double *) xubr_;
        ucl = (double *) ucl_;
        xucl = (double *) xucl_;
        ucc = (double *) ucc_;
        xucc = (double *) xucc_;
        ucr = (double *) ucr_;
        xucr = (double *) xucr_;
        ufl = (double *) ufl_;
        xufl = (double *) xufl_;
        ufc = (double *) ufc_;
        xufc = (double *) xufc_;
        ufr = (double *) ufr_;
        xufr = (double *) xufr_;

        memcpy(&kp, &kp_, sizeof(kp_));
        memcpy(&jp, &jp_, sizeof(jp_));
        memcpy(&i, &i_, sizeof(i_));
        memcpy(&imax, &imax_, sizeof(imax_));
        memcpy(&imin, &imin_, sizeof(imin_));
        memcpy(&ii, &ii_, sizeof(ii_));
        memcpy(&jmax, &jmax_, sizeof(jmax_));
        memcpy(&jmin, &jmin_, sizeof(jmin_));
        memcpy(&jj, &jj_, sizeof(jj_));
        memcpy(&kmax, &kmax_, sizeof(kmax_));
        memcpy(&kmin, &kmin_, sizeof(kmin_));
        memcpy(&kk, &kk_, sizeof(kk_));

        if (heapChunkSize > 0)
        {
            if (it < 1)
                oldBrk = mallocMemoryChunk(heapDataFile, minimumHeapAddress, maximumHeapAddress, heapChunkSize);
            else
                resetHeapData(minimumHeapAddress, maximumHeapAddress, heapChunkSize);
        }

        unsigned long long stack_length;
        register void* rsp __asm__ ("sp");
        register void *preRSP = rsp;
        void *storedTo;
        if (stackChunkSize > 0)
        {
            rsp = (void *) benStackPtrAddress;
            storedTo = saveCurrentStackToHeap(preRSP, &stack_length);
            char* buffer = loadBenchStackToCurrentStack(stackDataFile, benStackPtrAddress, stackChunkSize);
            memcpy(rsp, buffer, stackChunkSize);
        }

        tsc_start = rdtsc();

        loop();

        tsc_end = rdtsc();

        loop_cycles = tsc_end - tsc_start;
        if (loop_cycles < min_tsc_cycle)
            min_tsc_cycle = loop_cycles;

        if (stackChunkSize > 0)
        {
            rsp = preRSP;
            void *storeSpValAddr = getSavedAddress(storedTo);
            memcpy(rsp, storeSpValAddr, stack_length);
            restoreCurrentStack(storedTo, stack_length);
        }

    }

    if (heapChunkSize > 0)
        deleteMemoryMapping(oldBrk, maximumHeapAddress, heapChunkSize);

    printf("Simulation time: %" PRIu64 " cycles\n", min_tsc_cycle);

    printf("\n");
    printf("i: %d\n", i);
    printf("ii: %d\n", ii);
    printf("jj: %d\n", jj);
    printf("kk: %d\n", kk);

    void* ptrCheckSumVal = dummy(55, b, cbc, cbl, cbr, ccc, ccl, ccr, cfc, cfl, cfr, dbc, dbl, dbr, dcc, dcl, dcr, dfc, dfl, dfr, ubc, ubl, ubr, ucc, ucl, ucr, ufc, ufl, ufr, xcbc, xcbl, xcbr, xccc, xccl, xccr, xcfc, xcfl, xcfr, xdbc, xdbl, xdbr, xdcc, xdcl, xdcr, xdfc, xdfl, xdfr, xubc, xubl, xubr, xucc, xucl, xucr, xufc, xufl, xufr);
    printf("The checksum of the addresses of pointer variables is: %p \n", ptrCheckSumVal);

    return 0;
}
